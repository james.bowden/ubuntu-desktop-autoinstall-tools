#!/bin/sh

set -eu

msg () { 1>&2 printf -- '%b\n' "$@" ; }
die () { msg "$@" ; exit 1 ; }

while getopts ":i:o:" opt; do
	case $opt in
		i) in="$(realpath "$OPTARG")" ;;
		o) out="$(realpath "$OPTARG")" ;;
		\?)
		cat <<-EOF | while read -r l ; do msg "$l" ; done
		$0 -i <ISO> -o <ISO> -c <YML>
		\t -i Input ubuntu server ISO
		\t -o Output ubuntu server autoinstall ISO
		EOF
		exit 1 ;;
  esac
done

[ ! $# -eq 0 ] || eval "$0 -h"
[ -r "$in" ] || die "$in does not exist or read permission not granted"
[ -w "$(dirname "$out")" ] || die "$(dirname "$out") or write permission not granted"

cwd="$(mktemp -d --tmpdir="$(dirname "$out")" "$(basename "$out" .iso)".XXXXXXXXXX)"

src="$cwd/$(basename "$in" .iso)"
mbr="$cwd/$(basename "$in" .iso).mbr"
efi="$cwd/$(basename "$in" .iso).efi"

msg "$0: Unpacking ISO..."
xorriso -osirrox on -indev "$in" -extract / "$src"
chmod -R +w "$src"

msg "$0: Adding autoinstall kernel boot parameters..."
sed -i -e 's,---, autoinstall ---,g' "$src/boot/grub/grub.cfg"
sed -i -e 's,---, autoinstall ---,g' "$src/boot/grub/loopback.cfg"

#msg "Setting grub timeout to 1 second..."
#sed -i -e 's,^set timeout=[0-9]*,set timeout=1,g' "$src/boot/grub/grub.cfg"

msg "$0: Updating md5sum.txt..."
md5=$(md5sum "$src/boot/grub/grub.cfg" | cut -f1 -d ' ')
sed -i -e 's,^.*[[:space:]] ./boot/grub/grub.cfg,'"$md5"'  ./boot/grub/grub.cfg,' "$src/md5sum.txt"
md5=$(md5sum "$src/boot/grub/loopback.cfg" | cut -f1 -d ' ')
sed -i -e 's,^.*[[:space:]] ./boot/grub/loopback.cfg,'"$md5"'  ./boot/grub/loopback.cfg,' "$src/md5sum.txt"

# See https://askubuntu.com/a/1289505 for details on how we rebuild the remastered iso below

msg "$0: Extracting MBR template..."
dd if="$in" bs=1 count=446 of="$mbr"

msg "Extracting EFI partition image..."
skip=$(fdisk -l "$in" | grep '.iso2 ' | awk '{print $2}')
size=$(fdisk -l "$in" | grep '.iso2 ' | awk '{print $4}')
dd if="$in" bs=512 skip="$skip" count="$size" of="$efi"

msg "$0: Extracting boot preperation options..."
mkisofsopts="$(
  xorriso -indev "$in" -report_el_torito as_mkisofs \
      | sed -E '/^--grub2-mbr|^--modification-date|^-append_partition/d' \
      | tr '\n' ' '
)"

msg "$0: Packing new ISO..."
eval 'xorriso -as mkisofs -r -o' "$out" '--grub2-mbr' "$mbr" '-append_partition 2 0xEF' "$efi" "$mkisofsopts" "$src"